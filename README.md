极光JCore官方SDK ·官方伙伴发布·基础版依赖包


使用极光其它插件的时候需要按需求配合使用基础插件包。

目前以下我们发布三个插件都需要此依赖包，但其他所有插件都需要此依赖包，因此其他作者发布的插件打包时需要基础包也可以使用本依赖包，官方发布的依赖包已经版本过旧，请下载此新版依赖包：

极光分享
https://ext.dcloud.net.cn/plugin?id=4774、
极光短信
https://ext.dcloud.net.cn/plugin?id=4773
极光统计
https://ext.dcloud.net.cn/plugin?id=4750






SDK 说明
极光开发者服务 SDK 采用了模块化的使用模式，即一个核心（JCore）SDK + N 种业务（JPush，JAnalytics，...）SDK 的使用方式，方便开发者使用某一项服务或多项服务，极大的优化了多模块同时使用时功能模块重复的问题；开发者可以通过业务（JPush，JAnalytics，...）SDK 下载得到完整业务包，里面已经包含了 JCore SDK。如下



极光SDK基础组件。极光SDK使用都需要依赖该组件。

接入
1.将 nativeplugins/JG-JCore 导入项目对应位置。
2.项目 manifest.json 中接入 JG-JCore 插件。并配置好 appkey([极光 portal]() 注册分配)，channel 信息。



本插件下载地址：

https://ext.dcloud.net.cn/plugin?id=4752